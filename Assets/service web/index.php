<?php

$serverName = 'localhost';
$userName = 'root';
$password = '';
$dataBase = 'tower_defence';

if(isset($_GET['data']))
{
	$xmlDoc = new DOMDocument();
	$xmlDoc->loadXML(urldecode($_GET['data']));
	
	$actions = $xmlDoc->getElementsByTagName('action');
	
	if($actions->length == 1)
	{		
		switch($actions->item(0)->nodeValue)
			{
				case 'request':
				
				$mysqli = new mysqli($serverName, $userName, $password, $dataBase);
				
				if($mysqli->connect_errno)
				{
						echo 'bd error';
				}else{
					$sql = 'SELECT * FROM partie';
					
					$result = $mysqli->query($sql);
					
					if($result->num_rows > 0){
						
						echo '<?xml version="1.0" encoding="UTF-8"?><data>';
						
						while($row = $result->fetch_assoc()){
							echo '<partie>' .
							'<gold>' . $row['gold'] . '</gold>' .
							'<nb_enemy>' . $row['nb_enemy'] . '</nb_enemy>' .
							'<nb_tower>' . $row['nb_tower'] . '</nb_tower>' .
							'<score>' . $row['score'] . '</score>' .
							'<wave>' . $row['wave'] . '</wave>' .
							'<timestamp>' . $row['timestamp'] . '</timestamp>' .
							'<nom>' . $row['nom'] . '</nom>' .
							'</partie>';
						}
						echo '</data>';
				}else{
					echo 'no data';
				}
				}
				
				break;
				case 'send':
				
				$mysqli = new mysqli($serverName, $userName, $password, $dataBase);
				
				if($mysqli->connect_errno)
				{
						echo 'bd error';
				}else
				{
					$parties = $xmlDoc->getElementsByTagName('partie');
					
					foreach($parties as $partie)
					{						
						
						$sql = 'INSERT INTO partie(gold, nb_enemy, nb_tower, score, wave, nom) VALUES(';
						
						$gold = $partie->getElementsByTagName('gold')->item(0)->nodeValue;
						$nb_enemy = $partie->getElementsByTagName('nb_enemy')->item(0)->nodeValue;
						$nb_tower = $partie->getElementsByTagName('nb_tower')->item(0)->nodeValue;
						$score = $partie->getElementsByTagName('score')->item(0)->nodeValue;
						$wave = $partie->getElementsByTagName('wave')->item(0)->nodeValue;
						$nom = $partie->getElementsByTagName('nom')->item(0)->nodeValue;

						
						$sql .= $gold . ',';
						$sql .= $nb_enemy . ',';
						$sql .= $nb_tower . ',';
						$sql .= $score . ',';
						$sql .= $wave . ',';
						$sql .=  "'" . $nom . "'";
				
						$sql .= ');';						
							
						if($mysqli->query($sql) == TRUE)
						{
							echo 'partie saved';
						}else
						{
							echo 'error ' . $sql . '<br>' . $mysqli->error;
						}
					}
				}
				break;
			}
	}else
	{
		echo 'action mal definit';
	}

	
}else
{
	echo 'no data';
}

?>