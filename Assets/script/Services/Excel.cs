﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Table;

public class Excel : MonoBehaviour
{
    private List<Partie> parties;

    public void ExportExcel()
    {        
        StartCoroutine(exportAll());
    }

    private IEnumerator exportAll()
    {
        yield return StartCoroutine(LoadParties());
        try
        {
            using (var p = new ExcelPackage())
            {
                
                string path = Application.persistentDataPath + "/excelScoreboard.xlsx";

                var ws = p.Workbook.Worksheets.Add("new sheet");

                var range = ws.Cells[1, 2, parties.Count + 2, 8];
                
                var table = ws.Tables.Add(range, "table1");

                ws.Cells[1, 2].Value = "Nom";
                ws.Cells[1, 3].Value = "Gold";
                ws.Cells[1, 4].Value = "Nombre d'ennemis";
                ws.Cells[1, 5].Value = "Nombre de tours";
                ws.Cells[1, 6].Value = "Score";
                ws.Cells[1, 7].Value = "Vague";
                ws.Cells[1, 8].Value = "Date";
                ws.Cells[parties.Count + 2, 1].Value = "Total";

                for (int i = 0; i < parties.Count; i++)
                {
                    ws.Cells[i + 2, 2].Value = parties[i].Nom;
                    ws.Cells[i + 2, 3].Value = parties[i].Gold;
                    ws.Cells[i + 2, 4].Value = parties[i].NbEnemy;
                    ws.Cells[i + 2, 5].Value = parties[i].NbTower;
                    ws.Cells[i + 2, 6].Value = parties[i].Score;
                    ws.Cells[i + 2, 7].Value = parties[i].Wave;
                    ws.Cells[i + 2, 8].Value = parties[i].Date;
                }

                ws.Cells[parties.Count + 2, 3].Formula = "SUM(C2:C" + (parties.Count + 1) + ")";
                ws.Cells[parties.Count + 2, 4].Formula = "SUM(D2:D" + (parties.Count + 1) + ")";
                ws.Cells[parties.Count + 2, 5].Formula = "SUM(E2:E" + (parties.Count + 1) + ")";
                ws.Cells[parties.Count + 2, 6].Formula = "SUM(F2:F" + (parties.Count + 1) + ")";
                ws.Cells[parties.Count + 2, 7].Formula = "SUM(G2:G" + (parties.Count + 1) + ")";

                p.SaveAs(new FileInfo(path));

                System.Diagnostics.Process.Start(path);               
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    private IEnumerator LoadParties()
    {
        WebService webService = new WebService();

        yield return StartCoroutine(webService.LoadPartie(x => parties = x));
    }


}
