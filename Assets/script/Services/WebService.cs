﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using UnityEngine.Networking;

public class WebService
{
    private string url = "http://localhost/tower_defence/index.php?data=";

    public IEnumerator SavePartie(Partie partie)
    {
        string url = this.url + "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><action>send</action>" + partie.ToXML() + "</data>";

        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            Debug.Log(url);
            yield return webRequest.Send();

            if (webRequest.isError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
            }
        }
    }

    public IEnumerator LoadPartie(Action<List<Partie>> action)
    {
        string loadXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><data><action>request</action></data>";
        string url = this.url + loadXML;

        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            Debug.Log(url);
            yield return webRequest.Send();

            if (webRequest.isError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                string response = webRequest.downloadHandler.text;
                Debug.Log(response);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                var partie = doc.GetElementsByTagName("partie");
                List<Partie> listPartie = new List<Partie>();

                for (int i = 0; i < partie.Count; i++)
                {
                    Partie nouvellePartie = new Partie();
                    nouvellePartie.SetAttribut(partie[i].OuterXml);
                    listPartie.Add(nouvellePartie);
                }

                if(action != null)
                    action(listPartie);
            }
        }
    }
}