﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Enemy : MonoBehaviour {

    // Information sur l'ennemie
    public int hp;
    public int attack;
    public float speed;
	public int score;
    public int gold;
	private int wave;
	private int i = 0;

    // Chemin que l'ennemie doit prendre et son target
    private List<Vector2> paths;
    private Vector2 target;
    private bool reachedTarget = false;

    // Quelques GameObjects qui ont des interactions avec les ennemies
    private Animator animator;
    private GestionnaireCreationEnemy gestionnaire;
    private List<Tour> tours;
    private GameStats player;

    // Va chercher les GameObject 
    void Start () {
        animator = GetComponent<Animator>();
	    gestionnaire = GameObject.FindGameObjectWithTag("GestionnaireEnemy").GetComponent<GestionnaireCreationEnemy>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
		wave = player.GetWave();
		Scale();
        speed = UnityEngine.Random.Range(0.5f, 3f);
        paths = gestionnaire.GetPath();
        tours = new List<Tour>();
        NextTarget();
    }
	
	// Bouge l'ennemie et update l'animator si l'ennemie est arriver a son target alors on prend le prochain
	void Update () {
        if (reachedTarget == false)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, target, step);
            Vector2 movement = target - (Vector2)transform.position;
            if (movement.x > 0)
                animator.SetInteger("Direction", 1);
            if (movement.x < 0)
                animator.SetInteger("Direction", 2);
            if(movement.y > 0)
                animator.SetInteger("Direction", 3);
            if (movement.y < 0)
                animator.SetInteger("Direction", 4);

            if ((Vector2)transform.position == target)
            {
                reachedTarget = true;
            }
        }
        else
        {
            NextTarget();
            reachedTarget = false;
        }
	}

    
    // Prend la prochaine destination et si il y a plus alors on est arriver a la fin et on attack le joueur
    private void NextTarget()
    {
        if(paths.Count > i)
        {
            target = paths[i];
			i++;
        }
        else
        {
			player.ChangeHealth(-attack);
            hp = 0;
            Destroy(gameObject);
        }
        
    }

    public int GetAttack()
    {
        return this.attack;
    }

    public void TakeDamage(int amount)
    {
        this.hp -= amount;
        if (hp <= 0)
        {
			player.ChangeGold(gold);
            player.ChangeScore(score);
            player.KilledAnEnnemy();
            Destroy(gameObject);
        }
    }

    public void OnDestroy()
    {
        foreach (Tour tour in tours)
            tour.Destroyed(gameObject);

        // Enleve les ennemies mort de la liste du gestionnaire
        for (int i = 0; i < gestionnaire.ennemies.Count; i++)
        {
            Enemy e = gestionnaire.ennemies[i].GetComponent<Enemy>();
            if(e.hp <= 0)
            {
                gestionnaire.ennemies.RemoveAt(i);
            }
        }

        // Regarde si il ne reste plus d'ennemies
        gestionnaire.IsWaveOver();
    }

    public void AddTowerInRange(Tour tour)
    {
        tours.Add(tour);
    }

	private void Scale(){
		int scale = gestionnaire.getScale ();


		if (scale == 1) {
			hp = hp + (int)Math.Round (wave / 2f) * scale;
            hp = (int)(hp / speed);
			attack = attack + (int)Math.Round (wave / 2f) * scale;
			score = score * wave * scale;
			gold = gold * wave;
		}
		if (scale == 2) {
			hp = hp + (int)Math.Round (wave / 2f) * scale;
            hp = (int)(hp / speed);
            attack = attack + (int)Math.Round (wave / 2f) * scale;
			score = score * wave * scale;
			gold = gold * (int)Math.Round(wave / 2f) * scale;
		} else if(scale == 3){
			hp = hp + wave * scale;
            hp = (int)(hp / speed);
            attack = attack + (int)Math.Round (wave / 2f) * scale;
			score = score * wave * scale;
			gold = (gold * (int)Math.Round(wave / 4f) * scale) / 2;
		}
	}
}
