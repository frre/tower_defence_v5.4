﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GestionnaireCreationEnemy : MonoBehaviour {

    public List<GameObject> ennemies;
    public GameObject startWaveButton;
    public GameObject tourButton;
    public GameObject Enemy;
	private GameStats player;
    
	private float spawnDelay;
	private int scale = 1;
	private GameSettings gameSettings = GameSettings.instance;
    private int numberEnemiesSpawned;
    private int numberEnnemies;
    private List<Vector2> path;
    private Grille grille;
    private Vector3 spawn;
    private bool spawnIsOver = true;

    // Prend le path des ennemies et la position de départ
    void Start() {
        grille = GameObject.FindGameObjectWithTag("Grille").GetComponent<Grille>();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
        spawn = grille.GetStart();
		if (gameSettings.difficulty.Equals ("Normal")) {
			scale = 1;
			spawnDelay = 3f;
		} else if (gameSettings.difficulty.Equals ("Hard")){
			scale = 2;
			spawnDelay = 1.5f;
		} else if (gameSettings.difficulty.Equals ("Hardcore")){
			scale = 3;
			spawnDelay = 0.5f;
			player.ChangeHealth (-50);
		}
    }
		

    // Instancie les ennemies et arrete lorsque le nombre d'ennemies instancier = le nombre d'ennemies max de la vague
    void Spawn()
    {
        GameObject instance = Instantiate(Enemy, spawn, Quaternion.identity) as GameObject;
        instance.transform.SetParent(gameObject.transform);
        ennemies.Add(instance);
        numberEnemiesSpawned++;
        if(numberEnemiesSpawned == numberEnnemies)
        {
            CancelInvoke("Spawn");
            spawnIsOver = true;
        }
    }

    // Invoke la methode Spawn() en boucle avec le spawnDelay desirer
    void Spawner()
    {
        InvokeRepeating("Spawn", 1f, spawnDelay);
    }

    // Démarre la vague d'ennemies
    public void StartWave()
    {
        // Update le path, la wave et désactive les buttons
		player.NextWave();
        path = grille.GetPath();
        startWaveButton.SetActive(false);
        tourButton.SetActive(false);


        // Crée la wave
		if (spawnDelay > 0.5f)
			spawnDelay -= 0.1f;
		numberEnnemies = player.GetWave() * 2;
        numberEnemiesSpawned = 0;
        spawnIsOver = false;

        // Commence le spawn
        Spawner();
    }



    // La vague est terminé alors on réactive le button
    public void IsWaveOver()
    {
        if(ennemies.Count == 0 && spawnIsOver)
        {
            startWaveButton.SetActive(true);
            tourButton.SetActive(true);
        }
    }

	public int getScale(){
		return scale;
	}

    public List<Vector2> GetPath()
    {
        return path;
    }
		
}
