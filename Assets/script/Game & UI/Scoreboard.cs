﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Scoreboard : MonoBehaviour {

    public Text textScore;
    public Text text;


    // Use this for initialization
    void Start () {
        WebService webService = new WebService();
        StartCoroutine(webService.LoadPartie(AfficherPartie));
        textScore.text = "";
        text.text = "Nom".PadRight(40) + " " + "Vague" + " " + "Score";
	}

    private void AfficherPartie(List<Partie> list)
    {
        list.Sort((x,y) => y.Score.CompareTo(x.Score));
        for(int i = 0; i < Mathf.Min(8, list.Count); i++)
        {
            textScore.text += list[i].Nom.PadRight(40) + " " + list[i].Wave.ToString().PadLeft(6) + " " + list[i].Score.ToString().PadLeft(8) + "\n";
        }
    }

}
