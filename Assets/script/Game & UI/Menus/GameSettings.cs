﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSettings : MonoBehaviour {

    // Make Global
    public static GameSettings instance = null;

	void Awake () {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(transform.gameObject);

        DontDestroyOnLoad (transform.gameObject);
	}

	// Informations importantes
	public string difficulty;
	public Partie partieEnCours = new Partie ();
	private WebService webService = new WebService();

	public void ChangeDifficultyNormal(){
			instance.difficulty = "Normal";
	}

	public void ChangeDifficultyHard(){
            instance.difficulty = "Hard";
	}

	public void ChangeDifficultyHardcore(){
            instance.difficulty = "Hardcore";
		}

	public void SubmitGame(){
        StartCoroutine(webService.SavePartie(partieEnCours));
        SceneManager.LoadScene("Menu");
    }

}
