﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalScore : MonoBehaviour {

	private GameSettings gameSettings = GameSettings.instance;
	public GameObject score;
	private Text text;

	void Awake () {
		text = score.GetComponent<Text>();
		text.text += " " + gameSettings.partieEnCours.Score;
	}

}
