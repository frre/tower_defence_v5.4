﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;
using System.Xml;

public class Partie
{
    public int Gold { get; set; }
    public int NbEnemy { get; set; }
    public int NbTower { get; set; }
    public int Score { get; set; }
    public int Wave { get; set; }
    public string Date { get; set; }
    public string Nom { get; set; }

    public bool SetAttribut(string xml)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        try
        {
            if (doc.DocumentElement.Name == "partie")
            {
                if(doc.GetElementsByTagName("gold").Count > 0)
                    Gold = int.Parse(doc.GetElementsByTagName("gold")[0].InnerText);
                if (doc.GetElementsByTagName("nb_enemy").Count > 0)
                    NbEnemy = int.Parse(doc.GetElementsByTagName("nb_enemy")[0].InnerText);
                if (doc.GetElementsByTagName("nb_tower").Count > 0)
                    NbTower = int.Parse(doc.GetElementsByTagName("nb_tower")[0].InnerText);
                if (doc.GetElementsByTagName("score").Count > 0)
                    Score = int.Parse(doc.GetElementsByTagName("score")[0].InnerText);
                if (doc.GetElementsByTagName("wave").Count > 0)
                    Wave = int.Parse(doc.GetElementsByTagName("wave")[0].InnerText);
                if (doc.GetElementsByTagName("timestamp").Count > 0)
                    Date = doc.GetElementsByTagName("timestamp")[0].InnerText;
                if (doc.GetElementsByTagName("nom").Count > 0)
                    Nom = doc.GetElementsByTagName("nom")[0].InnerText;

                return true;
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return false;
    }

    public string ToXML()
    {
        XmlDocument doc = new XmlDocument();

        XmlElement partie = doc.CreateElement(string.Empty, "partie", String.Empty);
        doc.AppendChild(partie);

        XmlElement gold = doc.CreateElement(string.Empty, "gold", string.Empty);
        XmlText goldText = doc.CreateTextNode(Gold.ToString());
        partie.AppendChild(gold);
        gold.AppendChild(goldText);

        XmlElement nb_enemy = doc.CreateElement(string.Empty, "nb_enemy", string.Empty);
        XmlText nb_ennemyText = doc.CreateTextNode(NbEnemy.ToString());
        partie.AppendChild(nb_enemy);
        nb_enemy.AppendChild(nb_ennemyText);

        XmlElement nb_tower = doc.CreateElement(string.Empty, "nb_tower", string.Empty);
        XmlText nb_towerText = doc.CreateTextNode(NbTower.ToString());
        partie.AppendChild(nb_tower);
        nb_tower.AppendChild(nb_towerText);

        XmlElement score = doc.CreateElement(string.Empty, "score", string.Empty);
        XmlText scoreText = doc.CreateTextNode(Score.ToString());
        partie.AppendChild(score);
        score.AppendChild(scoreText);

        XmlElement wave = doc.CreateElement(string.Empty, "wave", string.Empty);
        XmlText waveText = doc.CreateTextNode(Wave.ToString());
        partie.AppendChild(wave);
        wave.AppendChild(waveText);

        XmlElement nom = doc.CreateElement(string.Empty, "nom", string.Empty);
        XmlText nomText = doc.CreateTextNode(Nom);
        partie.AppendChild(nom);
        nom.AppendChild(nomText);

        return doc.OuterXml;
    }
}