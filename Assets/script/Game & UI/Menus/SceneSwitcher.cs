﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {

	public void GoMenuScene()
	{
		SceneManager.LoadScene("Menu");
	}

	public void GoGameScene()
	{
		SceneManager.LoadScene("Game");
	}

	public void GoScoresScene()
	{
		SceneManager.LoadScene("Scores");
	}

	public void GoSettingsScene()
	{
		SceneManager.LoadScene("Settings");
	}

	public void ExitGame()
	{
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif

        #if UNITY_STANDALONE_WIN
        Application.Quit ();
        #endif
    }

}
