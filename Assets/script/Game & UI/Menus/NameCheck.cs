﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NameCheck : MonoBehaviour {

	public InputField UserName;
	private GameSettings gameSettings = GameSettings.instance;

	void Start(){
		UserName.characterLimit = 10;
	}

	public void CheckName(){
		if (UserName.text != "") {
            gameSettings.partieEnCours.Nom = UserName.text;
			gameSettings.SubmitGame ();
		} else {
			UserName.placeholder.GetComponent<Text>().text = "Besoin d'un nom";
		}
	}
}