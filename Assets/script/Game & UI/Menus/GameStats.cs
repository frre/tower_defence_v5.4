﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameStats : MonoBehaviour {

	private GameSettings gameSettings = GameSettings.instance;

    public int health;
    public int gold;
	public int score;
	public int wave;

	public GameObject waveUI;
	public GameObject healthUI;
	public GameObject scoreUI;
	public GameObject goldUI;

	private int nbEnnemyKilled = 0;
	private int nbTowerPlaced = 0;    

	public void KilledAnEnnemy(){
		nbEnnemyKilled++;
	}

	public void PlacedATower(){
		nbTowerPlaced++;
	}

    public void ChangeGold(int amount)
    {
        gold += amount;
		goldUI.GetComponent<GoldUI>().UpdateStat();
    }

	public void NextWave(){
		wave++;
		waveUI.GetComponent<WaveUI> ().UpdateStat ();
	}

	public int GetWave(){
		return wave;
	}

    public int GetGold()
    {
        return gold;
    }

	public int GetHealth(){
		return health;
	}

    public void ChangeHealth(int amount)
    {
        health += amount;
		healthUI.GetComponent<HealthUI>().UpdateStat();
		if (health <= 0) {
			GameOver();
		}
    } 

	public void ChangeScore(int amount){
		score += amount;
		scoreUI.GetComponent<ScoreUI>().UpdateStat();
	}

	public int GetScore(){
		return score;
	}

	private void GameOver(){
		gameSettings.partieEnCours.Gold = gold;
		gameSettings.partieEnCours.Score = score;
		gameSettings.partieEnCours.NbEnemy = nbEnnemyKilled;
		gameSettings.partieEnCours.NbTower = nbTowerPlaced;
        gameSettings.partieEnCours.Wave = wave;

	
		SceneManager.LoadScene ("Game Over");
	}

	// TODO: Methode qui retourne les stats de la partie
	/* Score (tranferer l'or en score a la fin, et le nombre de wave)
	 * Nombre total de gold recolter lors de la partie
	 * Nombre total d'ennemies tuer
	 * Nombre de wave survecue
	 * Nombre de tour placé
	 * 
	 */ 

}
