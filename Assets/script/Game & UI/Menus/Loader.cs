﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {

    public GameObject gameManager;

    void Awake()
    {
        if (GameSettings.instance == null)
            Instantiate(gameManager);
    }

}
