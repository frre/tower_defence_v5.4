﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

	private GameStats game;
	private Text text;

	void Awake() {
		game = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
		text = GetComponent<Text>();
		text.text = "Score " + game.GetScore();
	}

	public void UpdateStat () {
		text.text = "Score " + game.GetScore();
	}
}
