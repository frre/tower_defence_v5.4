﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public GameObject pauseMenu;

	void Start(){
		HidePause();
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(Time.timeScale == 1)
			{
				Time.timeScale = 0;
				ShowPause();
			} else if (Time.timeScale == 0){
				Time.timeScale = 1;
				HidePause();
			}
		}
	}

	// Recommence la partie
	public void Restart(){
		SceneManager.LoadScene("Game");
		Time.timeScale = 1;
	}

	// Continue la partie
	public void Resume(){
		Time.timeScale = 1;
		HidePause ();
	}

	//  Menu
	public void ExitGame(){
		SceneManager.LoadScene("Menu");
	}

	public void HidePause(){
		pauseMenu.SetActive (false);
	}

	public void ShowPause(){
		pauseMenu.SetActive (true);
	}
}
