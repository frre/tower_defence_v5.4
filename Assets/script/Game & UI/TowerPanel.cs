﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowerPanel : MonoBehaviour {

    public Button buttonVendre;
    private GestionnaireCreationTour gestionnaireTour;
    private GameObject tour;

	// Use this for initialization
	void Start () {
        gestionnaireTour = GameObject.FindGameObjectWithTag("GestionnaireTour").GetComponent<GestionnaireCreationTour>();
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
    
    public void SetTour(GameObject tour)
    {
        this.tour = tour;
        buttonVendre.onClick.AddListener(VendreTour);

    }

    void VendreTour()
    {
        if(tour != null)
        {
            gestionnaireTour.VendreTour(tour);
            tour = null;
            gameObject.SetActive(false);
        }
    }
}
