﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    private GameStats game;
    private Text text;

    void Awake () {
		game = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
        text = GetComponent<Text>();
		text.text = "" + game.GetHealth();
    }
	
	public void UpdateStat () {
		text.text = "" + game.GetHealth();
	}
}
