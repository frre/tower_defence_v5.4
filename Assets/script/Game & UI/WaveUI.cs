﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveUI : MonoBehaviour {

	private GameStats game;
	private Text text;

	void Awake () {
		game = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
        text = GetComponent<Text>();
		text.text = "Vague " + game.GetWave();
    }

	public void UpdateStat () {
		text.text = "Vague " + game.GetWave();
    }
}
