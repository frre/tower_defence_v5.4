﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{

    //TODO collision

    protected GameObject cible = null;
    protected Vector2 destination;
    protected bool actif = false;
    protected int domage = 0;
    protected float vitesse;

    // Use this for initialization
    void Start()
    {
        destination = transform.position;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (actif)
        {
            if (cible != null)
            {
                destination = cible.transform.position;
                gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, destination, Time.deltaTime * vitesse);
            }
            else
            {
                if (destination != (Vector2) gameObject.transform.position)
                {
                    gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, destination, Time.deltaTime * vitesse);
                }
                else
                {
                    Destroy(gameObject);   
                }
            }
        }
    }

    public void SetCible(GameObject cible, int domage, float vitesse)
    {
        this.cible = cible;
        this.domage = domage;
        this.vitesse = vitesse;
        actif = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (actif && collision.gameObject == cible)
        {
            collision.gameObject.GetComponent<Enemy>().TakeDamage(domage);
            Destroy(gameObject);
        }
    }
}
