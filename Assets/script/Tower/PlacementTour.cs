﻿using UnityEngine;
using System.Collections;

public class PlacementTour : MonoBehaviour {    
    
    //private Color color;
    private Grille grille;
    private GestionnaireCreationTour gestionnaire;

	// Use this for initialization
	void Start () {
    
        grille = GameObject.FindGameObjectWithTag("Grille").GetComponent<Grille>();
        gestionnaire = GameObject.FindGameObjectWithTag("GestionnaireTour").GetComponent<GestionnaireCreationTour>();
        
        MoveToPos(MousePosition());
    }

    // Update is called once per frame
    void Update ()
    {
        MoveToPos(MousePosition());

        if(Input.GetMouseButtonDown(0))
        {
            if(grille.EmptyPosition(gameObject.transform.position))
                AddTower();
        }else if(Input.GetMouseButtonDown(1))
        {
            Cancel();
        }
	}

    private void AddTower()
    {        
        if(gestionnaire.AddTower(gameObject))
            Destroy(this);
    }

    private void Cancel()
    {
        Destroy(gameObject);
    }

    private void MoveToPos(Vector3 pos)
    {
        pos.x = Mathf.Round(pos.x);
        pos.y = Mathf.Round(pos.y);
        pos.z = 0;

        gameObject.transform.position = pos;
    }

    private Vector3 MousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
