﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Construction : MonoBehaviour
{
    public int prix;

    public abstract void Demarer();

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetPrix()
    {
        return prix;
    }
}
