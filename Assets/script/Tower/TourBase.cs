﻿using UnityEngine;
using System.Collections;

public class TourBase : MonoBehaviour
{
    public GameObject range;

    private GestionnaireCreationTour gestionnaireTour;    

    private void Start()
    {
        gestionnaireTour = GameObject.FindGameObjectWithTag("GestionnaireTour").GetComponent<GestionnaireCreationTour>();
    }

    void OnMouseEnter()
    {
        range.SetActive(true);
    }

    void OnMouseExit()
    {
        range.SetActive(false);
    }

    void OnMouseDown()
    {
        gestionnaireTour.SelectTour(transform.parent.gameObject);        
    }
}
