﻿using System.Collections.Generic;
using UnityEngine;

public class Grille : MonoBehaviour
{
    public GameObject depart;
    public GameObject fin;

    private Vector2 pointDepart;
    private Vector2 pointFin;
    private List<Vector2> positions;
    private List<GameObject> tours;

    void Awake()
    {
        pointDepart = depart.transform.position;
        pointFin = fin.transform.position;

        positions = new List<Vector2>();
        tours = new List<GameObject>();        

        for (float x = pointDepart.x; x <= pointFin.x; x += (pointFin.x - pointDepart.x) / Mathf.Abs(pointFin.x - pointDepart.x))
            for (float y =  pointDepart.y; y <= pointFin.y; y += (pointFin.y - pointDepart.y) / Mathf.Abs(pointFin.y - pointDepart.y))
                positions.Add(new Vector2(x,y));     
    }
 
    //obtenir le point de départ des ennemies
    public Vector2 GetStart()
    {
        return pointDepart;
    }

    //obtenir le point d'arrivé des ennemies
    public Vector2 GetFinish()
    {
        return pointFin;
    }

    //retourne la liste des positions(vecteur 2) qui sont dans la grille
    public List<Vector2> GetPositions()
    {
        return positions;
    }

    //retourne la position des tours sur la grille
    public List<Vector2> GetToursPositions()
    {
        List<Vector2> vector2s = new List<Vector2>();

        foreach (var tour in tours)
            vector2s.Add(tour.transform.position);

        return vector2s;
    }

    //vérifie si la position est dans la grille et si elle contient une tour
    public bool EmptyPosition(Vector2 vector2)
    {
        return positions.Contains(vector2) && !GetToursPositions().Contains(vector2) && !(GetStart() == vector2) && !(GetFinish() == vector2);

    }

    //ajoute une tour sur la grille
    public bool AddTower(GameObject tour)
    {
        if (!tours.Contains(tour))
        {
            tours.Add(tour);
            return true;
        }
        return false;
    }

    public bool RemoveTower(GameObject tour)
    {
        return tours.Remove(tour);
    }

    //suprimer une tour de sur la grille
    public bool DeleteTower(GameObject tour)
    {
        return tours.Remove(tour);
    }

    //retourne une liste contenant le chemin a suivre
    public List<Vector2> GetPath()
    {
        Vector2 start = GetStart();
        Vector2 end = GetFinish();

        List<Vector2> closedSet = new List<Vector2>();

        List<Vector2> openSet = new List<Vector2>();
        openSet.Add(start);

        Dictionary<Vector2, Vector2> cameFrom = new Dictionary<Vector2, Vector2>();

        Dictionary<Vector2, float> gScore = new Dictionary<Vector2, float>();
        gScore[start] = 0;

        Dictionary<Vector2, float> fScore = new Dictionary<Vector2, float>();
        fScore[start] = Distance(start, end);

        while (openSet.Count > 0)
        {
            Vector2 current = Vector2.zero; ;
            float min = -1;

            foreach (var pair in fScore)
            {
                if (pair.Value < min || min == -1)
                {
                    if (openSet.Contains(pair.Key))
                    {
                        current = pair.Key;
                        min = pair.Value;
                    }
                }
            }

            if (current == end)
                return ReconstructPath(cameFrom, current);

            openSet.Remove(current);
            closedSet.Add(current);

            List<Vector2> neighbor = new List<Vector2>();
            List<Vector2> possibleNeighbor = new List<Vector2>();

            possibleNeighbor.Add(new Vector2(current.x - 1, current.y));
            possibleNeighbor.Add(new Vector2(current.x + 1, current.y));
            possibleNeighbor.Add(new Vector2(current.x, current.y - 1));
            possibleNeighbor.Add(new Vector2(current.x, current.y + 1));

            //tester si la position est valide
            foreach (Vector2 node in possibleNeighbor)
            {
                if (positions.Contains(node) && !GetToursPositions().Contains(node))
                    neighbor.Add(node);
            }

            foreach (Vector2 node in neighbor)
            {
                if (!closedSet.Contains(node))
                {
                    float score = gScore[current] + Distance(current, node);

                    if (!openSet.Contains(node))
                        openSet.Add(node);
                    else if (score >= gScore[node])
                        continue;

                    cameFrom[node] = current;
                    gScore[node] = score;
                    fScore[node] = gScore[node] + Distance(node, end);
                }
            }
        }

        return null;
    }


    private List<Vector2> ReconstructPath(Dictionary<Vector2, Vector2> cameFrom, Vector2 current)
    {
        List<Vector2> path = new List<Vector2>();

        path.Add(current);

        while (cameFrom.ContainsKey(current))
        {
            current = cameFrom[current];
            path.Add(current);
        }

        path.Reverse();

        return path;
    }

    private float Distance(Vector2 start, Vector2 end)
    {
        return Mathf.Sqrt(Mathf.Pow(start.x - end.x, 2) + Mathf.Pow(start.y - end.y, 2));
    }
}
