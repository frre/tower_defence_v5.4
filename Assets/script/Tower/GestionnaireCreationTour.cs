﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GestionnaireCreationTour : MonoBehaviour
{
    public List<GameObject> tours;
    public List<GameObject> button;
    public GameObject panelTour;

    private Grille grille;
    private GameStats player;

    private void Start()
    {
        grille = GameObject.FindGameObjectWithTag("Grille").GetComponent<Grille>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<GameStats>();
        CreationUiTour();
        panelTour.SetActive(false);
    }

    //cée le ui qui permet la création de tour
    private void CreationUiTour()
    {

    }

    //permet à l'utilisateur de placer la tour à l'endroit de son choix
    public void CreateTower(GameObject tour)
    {
        GameObject instance = Instantiate(tour, new Vector3(0, 0, 0f), Quaternion.identity) as GameObject;
        instance.transform.SetParent(gameObject.transform);
        instance.AddComponent<PlacementTour>();
        instance.transform.FindChild("Base").gameObject.SetActive(false);
    }

    //place la tour sur la grille pour qu'elle soit active
    public bool AddTower(GameObject tour)
    {
        Construction c = tour.GetComponentInChildren<Construction>();
        grille.AddTower(tour);

        if (player.GetGold() >= c.GetPrix() && grille.GetPath() != null)
        {
            player.ChangeGold(-c.GetPrix());
            player.PlacedATower();
            c.Demarer();
            tour.transform.FindChild("Base").gameObject.SetActive(true);
            return true;
        }
        else
        {
            grille.RemoveTower(tour);
            return false;
        }
    }

    public void VendreTour(GameObject tour)
    {
        Construction c = tour.GetComponentInChildren<Construction>();
        grille.RemoveTower(tour);
        player.ChangeGold(c.GetPrix()/2);
        Destroy(tour);
    }

    public void SelectTour(GameObject tour)
    {
        panelTour.SetActive(true);
        panelTour.GetComponent<TowerPanel>().SetTour(tour);        
    }
}
