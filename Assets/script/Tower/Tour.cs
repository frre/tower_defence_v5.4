﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tour : Construction
{
    private readonly string ENNEMI_TAG = "Enemy";

    public float distaceAttaque;
    public int domage;
    public float vitesseAttaque;
    public float vitesseRotation;
    public float vitesseProjectile;
    public GameObject projectile;
    public GameObject range;

    protected CircleCollider2D collider2D;
    protected Animator animator;

    public List<GameObject> ennemiAtaque;
    public bool active;
    public bool attaque;

    private void Start()
    {
        collider2D = GetComponent<CircleCollider2D>();
        animator = GetComponent<Animator>();

        collider2D.radius = distaceAttaque;
        range.transform.localScale = new Vector2(distaceAttaque*2, distaceAttaque*2);

        ennemiAtaque = new List<GameObject>();
        active = false;
        attaque = false;
    }

    private void Update()
    {
        if (active)
        {
            if (!attaque)
            {
                if (ennemiAtaque.Count > 0)
                {
                    GameObject instance = Instantiate(projectile, gameObject.transform.position, Quaternion.identity) as GameObject;
                    instance.transform.SetParent(gameObject.transform);

                    GameObject cible = ennemiAtaque[0];

                    instance.GetComponent<Projectile>().SetCible(cible, domage, vitesseProjectile);
                    
                    attaque = true;
                    animator.SetTrigger("attaque");
                    StartCoroutine(WaitAttackSpeed());
                }
            }
        }
    }

    public override void Demarer()
    {
        active = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ENNEMI_TAG)
        {
            ennemiAtaque.Add(collision.gameObject);
            collision.gameObject.GetComponent<Enemy>().AddTowerInRange(this);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ENNEMI_TAG)
        {
            ennemiAtaque.Remove(collision.gameObject);
        }
    }

    public void Destroyed(GameObject gameObject)
    {
        ennemiAtaque.Remove(gameObject);
    }

    protected IEnumerator WaitAttackSpeed()
    {
        yield return new WaitForSeconds(1 / vitesseAttaque);
        attaque = false;
    }
}
